import tkinter as tk
from tkinter import messagebox
import csv
import json
import beeminder
import os
import datetime
import settings as s
from settings import get_beeminder_info
from settings import set_beeminder_info
from functools import partial

class Entry:
	def __init__(self,title,part,source,mainpoint,connection,flashcards):
		self.title = title
		self.part = part
		self.source = source
		self.mainpoint = mainpoint
		self.connection = connection
		self.flashcards = flashcards

	def see(self):
		print ("Title = " + self.title)
		print ("Part = " + self.part)
		print ("Source = "+self.source)
		print ("Main Point = "+self.mainpoint.replace("\n",""))
		print ("Connection =" + self.connection.replace("\n",""))
		for x in self.flashcards:
			print ("Flash Card Type = " + x.type+" Question = " + x.question.replace("\n","") +" Answer = "+ x.answer.replace("\n",""))

	def to_dict(self):
		#Create a dictionary with all the information in the data structure
		dict_of_entry = {"Title" : self.title, "Apart of":self.part,"URL":self.source,"Main Point": self.mainpoint,"Connection To":self.connection}

		#This adds each flash card as a key pair, with the question being the key
		for x in self.flashcards:
			dict_of_entry[x.question] = x.answer
		return dict_of_entry

	def add_to_beeminder(self):
		#This prepares a comment for the Beeminder goals based on the title of the entry and what it is apart of
		comment = "I took notes on " + self.title + " Which is part of "+ self.part + ". Automatically Reported by Ceshi"

		#This uses the beeminder.py script from NArthur and pulls the Beeminder User Name, API Key and goal name from a seperate python file to prevent leaking secert credentials
		beeminder_user = beeminder.Beeminder()

		beeminder_info = get_beeminder_info()
		
		if beeminder_info != None:
			beeminder_user.set_username(beeminder_info['USER'])
			beeminder_user.set_token(beeminder_info['API_KEY'])
			beeminder_user.create_datapoint(beeminder_info['GOAL'],1,datetime.datetime.utcnow().timestamp(),comment)


class FlashCard:
	def __init__(self,question,answer,cardtype):
		self.question = question
		self.answer = answer
		self.type = cardtype

class MainWindow:
	def __init__(self, master):
		#Setup the Window
		self.master = master
		self.master.geometry("800x900")
		self.master.title("Ceshi")

		#Setup Menubar
		menubar = tk.Menu(self.master)

		#Setup File Menu
		filemenu = tk.Menu(menubar, tearoff=0)
		filemenu.add_command(label="New Q/A Card", command= lambda:self.new_qa_window(self.flashcards))
		filemenu.add_command(label="New Cloze Card", command= lambda: self.new_cloze_window(self.flashcards))
		filemenu.add_separator()
		filemenu.add_command(label="Submit Entry", command=lambda: self.save_entry())
		filemenu.add_command(label="Clear Entry", command= lambda: self.clear_entry())
		filemenu.add_separator()
		filemenu.add_command(label="Export Entires & Exit", command=lambda: self.export_entries())
		menubar.add_cascade(label="File", menu=filemenu)

		#Setup Edit Menu
		editmenu = tk.Menu(menubar, tearoff=0)
		flashcardmenu = tk.Menu(editmenu, tearoff=0)
		flashcardmenu.add_command(label="Edit Last Flashcard", command= lambda: self.new_last_flashcard_window(self.flashcards))
		flashcardmenu.add_command(label="Select Flashcard to Edit", command= None)
		editmenu.add_cascade(label="Flashcards", menu=flashcardmenu)
		editmenu.add_separator()
		editmenu.add_command(label="Preferences", command= lambda: self.new_preferences_window())
		menubar.add_cascade(label="Edit", menu=editmenu)

		#Setup Help Menu
		helpmenu = tk.Menu(menubar, tearoff=0)
		helpmenu.add_command(label="About", command= None)
		menubar.add_cascade(label="Help", menu=helpmenu)

		#Add Menubar to the window
		self.master.config(menu=menubar)

		#Setup Frame
		self.frame = tk.Frame(self.master)

		#Setup the Basic Text Inputs
		tk.Label(self.frame, text="Title").grid(row=0, column=0,columnspan=2)
		self.title = tk.Entry(self.frame, width=88)
		self.title.bind("<Control-a>", lambda event: select_all(event, self.title))
		self.title.grid(row=1,column=0, columnspan=2,pady=5)

		tk.Label(self.frame, text="Part of").grid(row=2,column=0,columnspan=2)
		self.part = tk.Entry(self.frame, width=88)
		self.part.bind("<Control-a>", lambda event: select_all(event, self.part))
		self.part.grid(row=3,column=0,columnspan=2,pady=5)

		tk.Label(self.frame, text="Source").grid(row=4,column=0,columnspan=2)
		self.source = tk.Entry(self.frame, width=88)
		self.source.bind("<Control-a>", lambda event: select_all(event, self.source))
		self.source.grid(row=5,column=0, columnspan=2,pady=5)

		#Setup the two large text entries
		tk.Label(self.frame, text="Main Point of the Article").grid(row=6,column=0,pady=5,columnspan=2)
		self.mainpoint = tk.Text(self.frame, width=100,height=10)
		self.mainpoint.bind("<Tab>", focus_next_window)
		self.mainpoint.bind("<Control-a>", lambda event: select_all_text(event, self.mainpoint))
		self.mainpoint.grid(row=7,column=0,columnspan=2,pady=5)
		self.mainpoint.configure(font=("Liberation Serif", 10))
		tk.Label(self.frame, text="Connection to Other Things You Already Know").grid(row=8,column=0,pady=5,columnspan=2)
		self.connection = tk.Text(self.frame,width=100,height=10)
		self.connection.bind("<Tab>", focus_next_window)
		self.connection.bind("<Control-a>", lambda event: select_all_text(event, self.connection))
		self.connection.grid(row=9,column=0,columnspan=2,pady=5)
		self.connection.configure(font=("Liberation Serif", 10))

		#Initalize an empty list to store all flashcards that will be made		
		self.flashcards = []

		#Setup the text area to display the flash cards
		tk.Label(self.frame, text="Flash Card Viewer").grid(row=10,column=0,pady=5,columnspan=2)
		self.display = tk.Text(self.frame,width=100,height=10)
		self.display.grid(row=11, column=0, columnspan=2,pady=5)
		self.display.configure(font=("Liberation Serif", 10))
		self.update_flashcard_display()

		#Initalize an empty list to store all entries that will be made
		self.entries = []
		
		#Setup all the buttons used in the main window
		tk.Button(self.frame, text="New Cloze Card",width=25,height=2,command = lambda: self.new_cloze_window(self.flashcards)).grid(row=12,column=0)
		tk.Button(self.frame,text="New Q/A Card", width=25, height=2, command= lambda: self.new_qa_window(self.flashcards)).grid(row=12,column=1)
		tk.Button(self.frame,text="Submit Entry",width=25, height=2, command=lambda: self.save_entry()).grid(row=13,column=0)
		tk.Button(self.frame,text="Export Entries",width=25,height=2, command=lambda: self.export_entries()).grid(row=13,column=1)

		#Pack the frame into the window
		self.frame.pack()

		#Set title text entry as first focus, as this is the first thing you type
		self.title.focus_set()

	def new_cloze_window(self, flashcards):
		#Creates new top level window of the ClozeWindow class
		self.new = tk.Toplevel(self.master)
		ClozeWindow(self.new, flashcards)

	def new_qa_window(self, flashcards):
		#Creates new top level window of the QAWindow class
		self.new = tk.Toplevel(self.master)
		QAWindow(self.new,flashcards)

	def new_last_flashcard_window(self, flashcards):
		#Creates new top level window of based on whether the last flashcard in the flashcards array is a cloze or QA cards
		self.new = tk.Toplevel(self.master)

		if flashcards[-1].type == "QA":
			QAEditWindow(self.new,flashcards)

		elif flashcards[-1].type == "cloze":
			ClozeEditWindow(self.new, flashcards)

	def new_preferences_window(self):
		self.new = tk.Toplevel(self.master)

		PreferenceWindow(self.new)

	def update_flashcard_display(self):
		self.display.config(state=tk.NORMAL)		
		#Clears the grid
		self.display.delete("1.0", tk.END)
		
		#Inserts the header
		self.display.insert("1.0", "Type\t\t|Field 1\t\t\t\t\t|Field 2\n")
		self.display.insert("2.0", "---------------------------------------------------------------------------------------------------\n")
		
		#Prints each flash card into the grid
		for x in range(0,len(self.flashcards)):
			self.display.insert("3.0",self.flashcards[x].type+"\t\t|"+self.flashcards[x].question[0:30]+"\t\t\t\t\t|"+self.flashcards[x].answer[0:30]+"\n")
			self.display.insert("4.0", "---------------------------------------------------------------------------------------------------\n")
		#Tells the display to update every 10000 milliseconds
		self.display.after(10000,self.update_flashcard_display)

		self.display.config(state=tk.DISABLED)

	def clear_entry (self):

		self.title.delete(0,tk.END)
		self.part.delete(0,tk.END)
		self.source.delete(0,tk.END)
		self.mainpoint.delete("1.0",tk.END)
		self.connection.delete("1.0",tk.END)
		self.flashcards.clear()

		self.title.focus_set()

		self.update_flashcard_display()

	def save_entry (self):
		#This saves each compoment of an entry into a local variable
		save_title = self.title.get()
		save_part = self.part.get()
		save_source = self.source.get()
		save_mainpoint = self.mainpoint.get("1.0",tk.END)
		save_connection = self.connection.get("1.0",tk.END)
		save_flashcards = self.flashcards.copy()

		#This creates a new entry, clears the form and saves the entry data structure into the entires list
		newEntry = Entry(save_title,save_part,save_source,save_mainpoint,save_connection,save_flashcards)
		
		#This send a data point to Beeminder that this entry was made!
		newEntry.add_to_beeminder()
		
		#This clears the entry fields, resets the flashcard array, and the flashcard view
		self.clear_entry()

		#This adds the new entry to the list
		self.entries.append(newEntry)

		#This prints everything in the entries list to see how many entries we have
		print ("============================= Printing "+ str(len(self.entries)) +" Entries ================================")
		for x in self.entries:
			x.see()
		print ("============================= Done Printing ================================")

	def write_flashcards(self,export_file,cardtype):
		export_writer = csv.writer(export_file, delimiter=",", quotechar='"', quoting=csv.QUOTE_MINIMAL)

		#This goes through all the flashcards in each entry and writes the row to the csv in a format that is determined by card type
		for x in self.entries:
			for y in x.flashcards:
				if cardtype == "QA" and y.type == "QA":
					export_writer.writerow([y.question.replace('\n',''),y.answer.replace('\n',''),x.title,x.part,x.source])
				elif cardtype == "cloze" and y.type == "cloze":
					export_writer.writerow([y.question.replace('\n',''),x.title,x.part,x.source])
	
	def export_entries(self):
		dir_path = os.path.dirname(os.path.realpath(__file__))
		#This will save the current entry if the title is not empty. This is a safe measure to prevent data loss if you mistakenly click the export button
		if self.title.get() != "":
			self.save_entry()
		#This writes all QA cards to a csv, then writes all cloze cards to a different csv, then dumps all entries to a JSON file
		with open(dir_path+'/qacards.csv', mode='a') as export_file:
			self.write_flashcards(export_file,"QA")
		with open(dir_path+'/clozecards.csv', mode='a') as export_file:
			self.write_flashcards(export_file,"cloze")
		with open(dir_path+'/entries.json',mode='a') as export_file:
			for x in self.entries:
				dict_of_entry = x.to_dict()
				json.dump(dict_of_entry,export_file, indent=4)

		#Close the Program
		self.master.destroy()

class ClozeWindow:
	def __init__(self, master, flashcards):
		#Setup the window
		self.master = master
		self.master.geometry("800x600")
		self.master.title("Add New Cloze Card")

		#Setup the frame
		self.frame = tk.Frame(self.master)

		#Setup cloze_input as a text area to enter cloze into
		self.cloze_input = tk.Text(self.frame)

		#Capture the tab key so you can go to the next element in the window instead of writing \t
		self.cloze_input.bind("<Tab>", focus_next_window)

		#Capture Ctrl+A so we can select all text in the box
		self.cloze_input.bind("<Control-a>", lambda event: select_all_text(event, self.cloze_input))

		#Pack the cloze_input text area on a seperate line because it will not save into the local variable
		self.cloze_input.pack()

		self.cloze_input.configure(font=("Liberation Serif", 12))

		#Add cloze_number variable to count up the number of clozes on a given card
		self.cloze_number = 0

		#Setup the buttons
		tk.Button(self.frame,text="Cloze", width=25, height=2,command= lambda: self.clozeify()).pack()
		tk.Button(self.frame,text="Submit",width=25,height=2,command =lambda: self.submit(flashcards)).pack()

		#Pack it all into the window
		self.frame.pack()

		#Set's the cloze input as the first focus so you can start typing right away
		self.cloze_input.focus_set()

	def clozeify(self):
		#This saves the coordinates of the users selection
		selected_coordinates = self.cloze_input.tag_ranges("sel")

		#This uses those coordinates to get the text the user selected
		selected_text = self.cloze_input.get(selected_coordinates[0],selected_coordinates[1])
		self.cloze_number = self.cloze_number + 1

		#This adds the cloze tags around the string, deletes the users selection and inserts the new string
		clozeifyed_text = "{{c"+str(self.cloze_number)+"::"+selected_text+"}}"
		self.cloze_input.delete(selected_coordinates[0],selected_coordinates[1])
		self.cloze_input.insert(selected_coordinates[0],clozeifyed_text)

	def submit(self, flashcards):
		#This gets all text in the textbox and creates a new flashcard from the information
		cloze = self.cloze_input.get("1.0", tk.END)
		flashcards.append(FlashCard(cloze,"","cloze"))

		#This deletes the ClozeWindow and returns the user to the main window
		self.master.destroy()
		
		for x in flashcards:
			print (x)

class ClozeEditWindow(ClozeWindow):
	def __init__(self, master, flashcards):
		super().__init__(master, flashcards)
		#Grab the last flashcard from the array
		lastFlashCard= flashcards.pop()

		#Now insert the info into the cloze input area so the user can edit it
		self.cloze_input.insert('1.0', lastFlashCard.question)

class QAWindow:
	def __init__(self, master, flashcards):
		#Setup the Window
		self.master = master
		self.master.geometry("800x600")
		self.master.title("Add New Q/A Card")

		#Setup the Frame
		self.frame = tk.Frame(self.master)

		#Setup the Question and Answer labels and textboxes
		tk.Label(self.frame,text="Question").pack()
		self.question_textarea = tk.Text(self.frame, width=50, height=10)
		self.question_textarea.bind("<Tab>",focus_next_window)
		self.question_textarea.bind("<Control-a>", lambda event: select_all_text(event, self.question_textarea))
		self.question_textarea.pack()
		self.question_textarea.configure(font=("Liberation Serif", 12))

		tk.Label(self.frame,text="Answer").pack()
		self.answer_textarea = tk.Text(self.frame, width=50, height=10)
		self.answer_textarea.bind("<Control-a>", lambda event: select_all_text(event, self.answer_textarea))
		self.answer_textarea.bind("<Tab>", focus_next_window)
		self.answer_textarea.pack()
		self.answer_textarea.configure(font=("Liberation Serif", 12))

		#Setup a warning label that could be populated with a warning later
		self.warning = tk.StringVar()
		tk.Label(self.frame, textvariable=self.warning).pack()

		#Setup the Submit Button 
		tk.Button(self.frame, text="Submit",width=25,height=2, command = lambda: self.submit(flashcards)).pack()

		#Pack the frame into the window
		self.frame.pack()

		#Set the question text area to focus so you can start typing the question right away
		self.question_textarea.focus_set()

	def submit(self,flashcards):
		#Get all the text in the question and answer boxes and create a new flashcard, which is appended to the flashcard list
		question_text = self.question_textarea.get("1.0",tk.END)
		answer_text = self.answer_textarea.get("1.0",tk.END)

		#Check to ensure either area is not empty, as there is no point submitting a blank card, or a card with only one side
		if answer_text.strip() != "" and question_text.strip() != "":
			flashcards.append(FlashCard(question_text.replace("\n",""),answer_text.replace("\n",""),"QA"))
			#Destory the Window
			self.master.destroy()
		else:
			self.warning.set("Please ensure both text areas have content before submitting.")
			self.frame.pack()

class QAEditWindow(QAWindow):
	def __init__(self, master, flashcards):
		super().__init__(master, flashcards)
		
		#Grab the last flashcard in the array
		lastFlashCard= flashcards.pop()

		#Now insert the flashcard into the Q/A text area so the user can edit them
		self.question_textarea.insert('1.0',lastFlashCard.question)
		self.answer_textarea.insert('1.0',lastFlashCard.answer)

class PreferenceWindow():
	def __init__(self, master):
		#Setup the Window
		self.master = master
		self.master.geometry("800x600")
		self.master.title("Preferences")

		#Setup the Frame
		self.frame = tk.Frame(self.master)

		#Grab the Beeminder Settings
		self.beeminder_info = get_beeminder_info()

		#Setup the Basic Text Inputs
		tk.Label(self.frame, text="User Name").grid(row=0, column=0,columnspan=2)
		self.user_entry = tk.Entry(self.frame, width=88)
		self.user_entry.bind("<Control-a>", lambda event: select_all(event, self.title))
		self.user_entry.grid(row=1,column=0, columnspan=2,pady=5)

		tk.Label(self.frame, text="Goal Name").grid(row=2,column=0,columnspan=2)
		self.goal_entry = tk.Entry(self.frame, width=88)
		self.goal_entry.bind("<Control-a>", lambda event: select_all(event, self.part))
		self.goal_entry.grid(row=3,column=0,columnspan=2,pady=5)

		tk.Label(self.frame, text="API Key").grid(row=4,column=0,columnspan=2)
		self.apikey_entry = tk.Entry(self.frame, width=88)
		self.apikey_entry.bind("<Control-a>", lambda event: select_all(event, self.source))
		self.apikey_entry.grid(row=5,column=0, columnspan=2,pady=5)

		tk.Button(self.frame, text="Submit",width=25,height=2, command = lambda: self.submit()).grid(row=6,column=0,columnspan=2)

		self.frame.pack()

		#Insert Current Beeminder Settings, If Any
		if self.beeminder_info != None:
			self.user_entry.insert(0,self.beeminder_info["USER"])
			self.goal_entry.insert(0,self.beeminder_info["GOAL"])
			self.apikey_entry.insert(0,self.beeminder_info["API_KEY"])

	def submit(self):
		new_user = self.user_entry.get()
		new_goal = self.goal_entry.get()
		new_api_key = self.apikey_entry.get()

		change = 0

		if self.beeminder_info["USER"] != new_user:
			self.beeminder_info["USER"] = new_user
			change = 1

		if self.beeminder_info["GOAL"] != new_goal:
			self.beeminder_info["GOAL"] = new_goal
			change = 1

		if self.beeminder_info["API_KEY"] != new_api_key:
			self.beeminder_info["API_KEY"] = new_api_key
			change = 1

		if change == 1:
			set_beeminder_info(True, self.beeminder_info["USER"], self.beeminder_info["API_KEY"], self.beeminder_info["GOAL"])

		#Destory the Window
		self.master.destroy()


def focus_next_window(event):
	#This function is called when pressing tab on a text area, so it will go to the next text area instead of printing a \t
	event.widget.tk_focusNext().focus()
	return ("break")

def select_all(event, entry_object):
	#This function is called when pressing ctrl+a on an Entry object, and selects all text in the object
	entry_object.selection_range(0,tk.END)
	return ("break")

def select_all_text(event, text_object):
	#This function is called when pressing ctrl+a on an Text object, and selects all text in the object
	text_object.tag_add('sel','1.0',tk.END)
	return ("break")

window = tk.Tk()

app = MainWindow(window)

window.mainloop()