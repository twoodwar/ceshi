import configparser
import os 

def get_beeminder_info():
	config = configparser.ConfigParser()
	dir_path = os.path.dirname(os.path.realpath(__file__))
	config.read(dir_path+"/config.ini")

	if len(config.sections()) != 0:

		beeminder = config['BEEMINDER']

		if beeminder.getboolean("USE"):
			beeminder_info = dict(USER="", API_KEY="", GOAL="")

			beeminder_info['USER']= config['BEEMINDER']['USER']
			beeminder_info['API_KEY']= config['BEEMINDER']['API_KEY']
			beeminder_info['GOAL']= config['BEEMINDER']['GOAL']

			return beeminder_info
		else:
			return None
	else:
		return None

def set_beeminder_info(ENABLED, USER,API_KEY,GOAL):
	dir_path = os.path.dirname(os.path.realpath(__file__))
	config = configparser.ConfigParser()
	config['BEEMINDER'] = { 'USE': ENABLED,
							'USER': USER,
							'API_KEY': API_KEY,
							'GOAL': GOAL}

	with open(dir_path+"/config.ini", 'w') as configfile:
		config.write(configfile)

def main():
	beeminder_info = get_beeminder_info()

	for k,v in beeminder_info.items():
		print (k, v)

if __name__  == "__main__":
	main()