# Ceshi
A desktop application written in Python for taking notes and creating flashcards while studying. Includes [Beeminder](https://www.beeminder.com) integration. Exports entries to CSV and JSON files.

## Getting Started

### Prerequisites

You will need:
* Python 3.9

Optional - If you want to integrate with Beeminder:
* A [Beeminder](https://www.beeminder.com) Account

### Installing

Clone the repository from Gitlab

```
git clone https://gitlab.com/twoodwar/ceshi.git
```

#### Installing Dependencies
After cloning the repo, use requirements.txt to download the required packages.
```
pip install -r requirements.txt
```

### Usage
Run and use!
```
python Ceshi.py
```
#### Beeminder Support
Edit the config.ini file with your Beeminder API key and the name of the goal you want Ceshi to control.
```
[BEEMINDER]
use = <--True or False: Turn on or off Beeminder Support-->
user = <--Insert your Beeminder Username here-->
api_key = <--Insert your Beeminder API Key here-->
goal = <--Insert the name of the Beeminder Goal for Ceshi here-->
```
Download [NArthur](https://github.com/narthur)'s [beeminder.py](https://github.com/narthur/pyminder/blob/master/pyminder/beeminder.py) into the same directory Ceshi.py lives.
```
curl https://github.com/narthur/pyminder/blob/master/pyminder/beeminder.py > beeminder.py
```
In Beeminder, you should create a Do More goal. Each entry in Ceshi created will result in a +1 to the connected Do More goal. The goal needs to be created first, Ceshi will not create the goal for you.

## Versioning

This repo uses [SemVer](http://semver.org/) for versioning. 

## Authors

* **Tyler Woodward** - [twoodwar](https://gitlab.com/twoodwar)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgements

* Thank you to [NArthur](https://github.com/narthur) for [beeminder.py](https://github.com/narthur/pyminder/blob/master/pyminder/beeminder.py), which Ceshi uses to communicate with the Beeminder API
* Inspired by [POLAR's](https://getpolarized.io/) ability to import flash cards into Anki
* Inspired by [Make It Stick: The Science of Successful Learning](https://www.goodreads.com/book/show/18770267-make-it-stick) by *Peter C. Brown, Henry L. Roediger III, and Mark A. McDaniel*
